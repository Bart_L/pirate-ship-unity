The point of the game was to implement movement of the pirate ship. The ship can speed up and is swinging left and right. If player is not controlling the ship, it is flowing with water.
Cannons can be shot only if camera is looking strictly left or right. When a ball touches the surface, particles are beign shown.

CONTROLS :
WSAD - movement
Mouse - camera
SPACE = cannonball (must be looking left or right)

[DEMO HERE](http://pp43694.wsbpoz.solidhost.pl/test/pirateship/)