﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CameraLookDirection { FORWARD, RIGHT, BACKWARD, LEFT }


public class Camera : MonoBehaviour
{

    public Transform ObjectToTrack;
    public Vector3 Delta;

    private float cameraAngle = 0;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
        // look left and right
         var mouseDelta = Input.GetAxis("Mouse X");
	    cameraAngle += mouseDelta;

	    var quaternion = Quaternion.Euler(0, cameraAngle, 0);

	    var position = ObjectToTrack.position 
	        + ObjectToTrack.rotation * quaternion * Delta;
	    transform.position = position;
   
        transform.LookAt(ObjectToTrack);

        // change camera postion by 10deg up

        transform.rotation *= Quaternion.Euler(-10f, 0, 0);

	    //Debug.Log(GetCameraLookDirection()); //
	}

    public CameraLookDirection GetCameraLookDirection()
    {
        var angle = Mathf.DeltaAngle(0, cameraAngle);
        if (-45 < angle && angle < 45)
        {
            return CameraLookDirection.FORWARD;
        }
        if (45 < angle && angle < 135)
        {
            return CameraLookDirection.RIGHT;
        }

        if (-135 < angle && angle < -45)
        {
            return CameraLookDirection.LEFT;
        }

        return CameraLookDirection.BACKWARD;
    }
}
