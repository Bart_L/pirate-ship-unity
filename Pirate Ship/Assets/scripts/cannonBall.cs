﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cannonBall : MonoBehaviour
{

    public GameObject WaterParticles;
    public GameObject TerrainPatricles;

	// Use this for initialization
	void Start ()
	{
		Destroy(gameObject, 10f);
	}

    private void OnCollisionEnter(Collision collision)
    {
        // get id of an object which the cannon ball hit
        var layerID = collision.collider.gameObject.layer;
        var layerName = LayerMask.LayerToName(layerID);

        GameObject particlesObject = null;

        if (layerName == "Water")
        {
            particlesObject = WaterParticles;
        }

        if (layerName == "Terrain")
        {
            particlesObject = TerrainPatricles;
        }

        var positionOfContact = collision.contacts[0].point;

        Instantiate(particlesObject, positionOfContact, Quaternion.identity);
        Debug.Log(layerName);
        Destroy(gameObject);
    }
}
