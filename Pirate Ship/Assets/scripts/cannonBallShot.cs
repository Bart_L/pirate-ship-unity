﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class cannonBallShot : MonoBehaviour
{

    public GameObject CannonBallPrefab;

    public Vector3 LeftSpawnPosition;
    public Vector3 LeftShootDirection;


    public Vector3 RightSpawnPosition;
    public Vector3 RightShootDirection;

    // max time of shooting
    public float ShootPeriod = 1f;
    // last cannonball shot
    private float LastShootTime = 0;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
	    if (!Input.GetKeyDown(KeyCode.Space)) return;

	    if (Time.timeSinceLevelLoad - LastShootTime < ShootPeriod) return;
        // if not, lastshoottime is timesince level load
	    LastShootTime = Time.timeSinceLevelLoad;

	    var camera = FindObjectOfType<Camera>();
	    var direction = camera.GetCameraLookDirection();

	    if (direction == CameraLookDirection.FORWARD || direction == CameraLookDirection.BACKWARD) return;

	    var lookLeft = direction == CameraLookDirection.LEFT;

	    var SpawnPosition = lookLeft ? LeftSpawnPosition : RightSpawnPosition;
	    var ShootDirection = lookLeft ? LeftShootDirection : RightShootDirection;


	    var ball = Instantiate(CannonBallPrefab);
	    ball.transform.position = transform.position + transform.rotation * SpawnPosition;

        // direction and velocity
	    var rigidbody = ball.GetComponent<Rigidbody>();
	    rigidbody.velocity = transform.rotation * ShootDirection;
	}

}
